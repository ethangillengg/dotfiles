{
  imports = [
    ./thunderbird.nix
    ./mail.nix
    ./aerc.nix
    # ./neomutt.nix

    # Pass feature is required
    ../pass
  ];
}
